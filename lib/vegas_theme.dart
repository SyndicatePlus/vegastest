import 'package:flutter/material.dart';

class ThemeColor {
  static _VegasThemeColor _color = _VegasThemeColor();

  static Color get primary => _color.primary;
  static Color get secondary => _color.secondary;
  static Color get background => _color.background;
  static Color get secondaryBackground => _color.secondaryBG;
  static Color get primaryButton => _color.primaryButton;
  static Color get secondaryButton => _color.secondaryButton;
}

class _VegasThemeColor {
  final Color primary = Color.fromARGB(255, 196, 33, 39);
  final Color secondary = Color.fromARGB(255, 147, 24, 29);
  final Color background = Colors.black;
  final Color secondaryBG = Color.fromARGB(255, 42, 45, 52);

  final Color primaryButton = Color.fromARGB(255, 240, 191, 76);
  final Color secondaryButton = Color.fromARGB(255, 250, 183, 1);

  final Color successColor = Color.fromARGB(255, 33, 196, 76);
  final Color errorColor = Color.fromARGB(255, 255, 69, 75);
  final Color demoPrimary = Color.fromARGB(255, 252, 145, 58);
  final Color demoSecondaryVariant = Color.fromARGB(255, 189, 108, 43);
  final Color disabledColor = Color.fromARGB(255, 212, 213, 214);
  final Color highEmphasisColor = Color.fromARGB(255, 84, 87, 92);
  final Color mediumEmphasisColor = Color.fromARGB(255, 148, 149, 153);
  final Color lowEmphasisColor = Color.fromARGB(255, 239, 239, 239);
}
