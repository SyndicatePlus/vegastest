import 'package:flutter/material.dart';
import 'package:vegas_test/nav.dart';
import 'package:vegas_test/vegas_theme.dart';

class CityTileData {
  String cityName;
  String stateCode;
  String countryName;
  String cityID;

  CityTileData(this.cityName, this.stateCode, this.countryName, this.cityID);
}

class CityTile extends StatefulWidget {
  final CityTileData cityData;

  const CityTile(this.cityData);

  @override
  State<StatefulWidget> createState() => _CityTile();
}

class _CityTile extends State<CityTile> {
  _onTapHandler() {
    final String cID = super.widget.cityData.cityID;
    final String cName = super.widget.cityData.cityName;

    Map args = {'id': cID, 'cityName': cName};

    Navigation.goto(NavPath.HOTELS, args: args);
  }

  @override
  Widget build(BuildContext context) {
    final cData = super.widget.cityData;

    String fullTitle = cData.cityName;
    if (cData.stateCode != '') fullTitle += ', ' + cData.stateCode;

    // Card
    Card card = Card(
        child: ListTile(
            onTap: _onTapHandler,
            leading: Icon(Icons.map),
            title: Text(fullTitle),
            subtitle: Text(cData.countryName)));

    return card;
  }
}
