import 'package:flutter/material.dart';

class HotelData {
  String hotelName;
  String imgUrl;
  String address;
  String zip;

  HotelData(this.hotelName, this.imgUrl, this.address, this.zip);
}

class HotelTile extends StatefulWidget {
  final HotelData hotelData;

  const HotelTile(this.hotelData);

  @override
  State<StatefulWidget> createState() => _HotelTile();
}

class _HotelTile extends State<HotelTile> {
  _onTapHandler() {}

  @override
  Widget build(BuildContext context) {
    final hData = super.widget.hotelData;

    ListTile lt = ListTile(
        onTap: _onTapHandler,
        leading: Image.network('https:' + hData.imgUrl),
        title: Text(hData.hotelName),
        subtitle: Text(hData.address + ' ' + hData.zip));

    // Card
    Card card = Card(child: lt);

    return card;
  }
}
