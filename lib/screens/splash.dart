import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:vegas_test/nav.dart';
import 'package:vegas_test/vegas_theme.dart';

class Splash extends StatelessWidget {
  const Splash({Key key}) : super(key: key);

  void _navigate() {
    print('SPLASH NAV');
    Navigation.goto(NavPath.CITY_SEARCH);
  }

  @override
  Widget build(BuildContext context) {
    print('Build Splash');
    SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);

    //Future.delayed(const Duration(milliseconds: 600), _navigate);

    Image img = Image.asset("assets/VegasCom.jpg");

    Container cont = Container(
      child: img,
      color: ThemeColor.background,
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
    );

    return cont;
  }
}
