import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:vegas_test/nav.dart';
import 'package:vegas_test/vegas_theme.dart';
import 'package:vegas_test/widgets/hotel_tile.dart';
import 'package:xml/xml.dart';
import 'package:http/http.dart' as http;

class Hotels extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _HotelsState();
}

class _HotelsState extends State<Hotels> {
  String _cityID;
  String _cityName;

  List<HotelData> _hotelsData;

  // TODO
  // Setup Stream to connect to hotels_block.dart
  // Stream<String> cityID()
  // {
  //  Stream.fromFuture(future)
  // }

  _onGoBack() {
    _cityID = null;
    _cityName = null;
    _hotelsData = [];
    Navigation.goBack();
  }

  Future _getHotels() async {
    String url = "api.rezserver.com";
    String path = "api/hotel/getResultsWithCacheV2";

    Map<String, dynamic> qParams = {};
    qParams['format'] = 'xml';
    qParams['api_key'] = 'aee0442f2f656cba59e5e5fc5e341950';
    qParams['refid'] = '2999';
    qParams['city_id'] = _cityID;
    qParams['limit'] = '5';

    final response = await http.get(Uri.https(url, path, qParams));

    // Parse XML to Hotel Info.
    final XmlDocument xml = XmlDocument.parse(response.body);
    Iterable<XmlElement> elements = xml.findAllElements('hotel');

    _hotelsData = [];
    elements.map((e) {
      final hotelName = e.findElements('name').first.text;
      final thumbNail = e.findElements('thumbnail').first.text;
      final address = e.findAllElements('address_line_one').first.text;
      final zip = e.findAllElements('zip').first.text;
      _hotelsData.add(HotelData(hotelName, thumbNail, address, zip));
    }).toList();

    // Update State.
    setState(() {});
  }

  @override
  void initState() {
    super.initState();

    print('HOLY');
    Map args = Get.arguments;
    _cityID = args['id'];
    _cityName = args['cityName'];

    _getHotels();
  }

  @override
  Widget build(BuildContext context) {
    print('Build City');

    // Recieve arguments through GetX.
    Map args = Get.arguments;
    _cityID = args['id'];
    _cityName = args['cityName'];

    return new Scaffold(
        body: _buildBody(),
        appBar: new AppBar(
            leading: IconButton(
                icon: Icon(Icons.arrow_back),
                color: Colors.white,
                onPressed: _onGoBack),
            title: new Text(_cityName),
            centerTitle: true,
            backgroundColor: ThemeColor.secondaryBackground));
  }

  Widget _buildBody() {
    List<Widget> columnList = [];

    if (_hotelsData != null && _hotelsData.length > 0) {
      ListView lView = ListView.builder(
          scrollDirection: Axis.vertical,
          shrinkWrap: true,
          itemCount: _hotelsData.length,
          itemBuilder: (context, index) {
            HotelData cData = _hotelsData[index];
            return HotelTile(cData);
          });

      columnList.add(Expanded(child: lView));
    }

    // Main Container
    return Container(
      child: Column(children: columnList),
      padding: const EdgeInsets.all(8.0),
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      color: Colors.black,
    );
  }
}
