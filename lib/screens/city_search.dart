import 'package:flutter/material.dart';
import 'package:vegas_test/vegas_theme.dart';
import 'package:vegas_test/widgets/city_search_tile.dart';
import 'package:xml/xml.dart';
import 'package:http/http.dart' as http;

class CitySearch extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _CitySearchState();
}

class _CitySearchState extends State<CitySearch> {
  final TextEditingController _controller = TextEditingController();

  List<CityTileData> _cityData;

  _cityTap() {}

  @override
  Widget build(BuildContext context) {
    print('Build City Search.');
    return new Scaffold(
        body: _buildBody(),
        appBar: new AppBar(
            title: new Text("City Search"),
            automaticallyImplyLeading: false,
            centerTitle: true,
            backgroundColor: ThemeColor.secondaryBackground));
  }

  Future _onSubmitted() async {
    String url = "api.rezserver.com";
    String path = "api/hotel/getAutoSuggestV2";

    Map<String, dynamic> qParams = {};
    qParams['format'] = 'xml';
    qParams['api_key'] = 'aee0442f2f656cba59e5e5fc5e341950';
    qParams['refid'] = '2999';
    qParams['get_cities'] = '1';
    qParams['string'] = _controller.text;

    print(_controller.text);

    final response = await http.get(Uri.https(url, path, qParams));

    // Parse XML to _cityData.
    final XmlDocument xml = XmlDocument.parse(response.body);
    Iterable<XmlElement> elements = xml.findAllElements('city');

    _cityData = [];
    elements.map((e) {
      final c = e.findElements('city');

      if (c != null && c.isNotEmpty) {
        final cityName = e.findElements('city').first.text;
        final stateCode = (e.findElements('state_code').isNotEmpty)
            ? e.findElements('state_code').first.text
            : '';

        final country = e.findElements('country').first.text;
        final cityID = e.findElements('cityid_ppn').first.text;
        _cityData.add(CityTileData(cityName, stateCode, country, cityID));
      }
    }).toList();

    setState(() {});
  }

  Widget _buildBody() {
    List<Widget> columnList = [];

    // Text Field
    TextField cityTextField = TextField(
        controller: _controller,
        style: TextStyle(fontSize: 26, color: Colors.black),
        decoration: InputDecoration(
            hintText: "",
            labelText: "City Name",
            labelStyle: TextStyle(fontSize: 20, color: Colors.black87),
            border: InputBorder.none,
            fillColor: Colors.white,
            filled: true),
        obscureText: false,
        maxLength: 30);
    columnList.add(cityTextField);

    // Button - UPDATE TO TextButton 2021.
    TextButton searchButton = TextButton.icon(
      icon: Icon(Icons.search, color: Colors.white),
      label: Text('SEARCH'),
      style: TextButton.styleFrom(
          primary: Colors.white,
          backgroundColor: ThemeColor.secondaryButton,
          textStyle: TextStyle(fontSize: 22)),
      onPressed: _onSubmitted,
    );

    Container buttonContainer = Container(
      child: searchButton,
      width: 200,
      height: 40,
    );
    columnList.add(buttonContainer);

    // Add Vertical Space in Column.
    columnList.add(SizedBox(height: 20));

    // Scrollable List View.
    if (_cityData != null && _cityData.length > 0) {
      ListView lView = ListView.builder(
          scrollDirection: Axis.vertical,
          shrinkWrap: true,
          itemCount: _cityData.length,
          itemBuilder: (context, index) {
            CityTileData cData = _cityData[index];
            return CityTile(cData);
          });

      columnList.add(Expanded(child: lView));
    }

    // Main Container
    return Container(
      child: Column(children: columnList),
      padding: const EdgeInsets.all(8.0),
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      color: Colors.black,
    );
  }
}
