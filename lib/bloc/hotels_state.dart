import 'package:equatable/equatable.dart';
import 'package:vegas_test/widgets/hotel_tile.dart';

abstract class HotelsState extends Equatable {
  const HotelsState();

  @override
  List<Object> get props => [];
}

class HotelsIntialState extends HotelsState {}

class HotelsDataState extends HotelsState {
  const HotelsDataState(this.hotelData);

  // Recieved Data.
  final HotelData hotelData;

  @override
  List<Object> get props => [hotelData];
}
