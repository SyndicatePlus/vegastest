import 'package:equatable/equatable.dart';
import 'package:vegas_test/widgets/hotel_tile.dart';

abstract class HotelsEvent extends Equatable {
  //
  const HotelsEvent();

  @override
  List<Object> get props => [];
}

class LoadHotelsEvent extends HotelsEvent {
  const LoadHotelsEvent(this.hotelData);

  final HotelData hotelData;

  @override
  List<Object> get props => [hotelData];
}
