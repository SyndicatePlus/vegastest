import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:vegas_test/bloc/hotels_event.dart';
import 'package:vegas_test/bloc/hotels_state.dart';
import 'package:vegas_test/widgets/hotel_tile.dart';

import 'package:xml/xml.dart';
import 'package:http/http.dart' as http;

class HotelsBloc extends Bloc<HotelsEvent, HotelsState> {
  //
  static final HotelsBloc _hotelsBlocSingleton = HotelsBloc();

  factory HotelsBloc() {
    return _hotelsBlocSingleton;
  }

  //HotelsBloc._internal(): super(UnHotelsState(0));
  StreamSubscription _subscription;
  List<HotelData> _hotelsData;
  String _cityID;

  @override
  Future<void> close() async {
    await super.close();
  }

  Future _getHotelData() async {
    String url = "api.rezserver.com";
    String path = "api/hotel/getResultsWithCacheV2";

    Map<String, dynamic> qParams = {};
    qParams['format'] = 'xml';
    qParams['api_key'] = 'aee0442f2f656cba59e5e5fc5e341950';
    qParams['refid'] = '2999';
    qParams['city_id'] = _cityID;
    qParams['limit'] = '5';

    final response = await http.get(Uri.https(url, path, qParams));

    // Parse XML to Hotel Info.
    final XmlDocument xml = XmlDocument.parse(response.body);
    Iterable<XmlElement> elements = xml.findAllElements('hotel');

    _hotelsData = [];
    elements.map((e) {
      final hotelName = e.findElements('name').first.text;
      final thumbNail = e.findElements('thumbnail').first.text;
      final address = e.findAllElements('address_line_one').first.text;
      final zip = e.findAllElements('zip').first.text;
      _hotelsData.add(HotelData(hotelName, thumbNail, address, zip));
    }).toList();
  }

  @override
  Stream<HotelsState> mapEventToState(HotelsEvent event) async* {
    //
    if (event is HotelsEvent) {
      //
      await _getHotelData();

      // _ticker.tick().listen((tick) => add(_TickerTicked(tick)));
    }
    yield state;
  }
}
