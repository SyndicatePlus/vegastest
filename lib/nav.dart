import 'package:get/get.dart';
import 'package:vegas_test/screens/splash.dart';
import 'package:vegas_test/screens/city_search.dart';
import 'package:vegas_test/screens/hotels.dart';

class NavPath {
  static const SPLASH = '/splash';
  static const CITY_SEARCH = '/citySearch';
  static const HOTELS = '/hotels';
  static const DETAILS = '/details';

  static List<GetPage> _pages;

  static List<GetPage> pages() {
    if (_pages != null) return _pages;

    //Transition tr = Transition.leftToRight;

    _pages = [];
    _pages.add(GetPage(name: NavPath.SPLASH, page: () => Splash()));
    _pages.add(GetPage(name: NavPath.CITY_SEARCH, page: () => CitySearch()));
    _pages.add(GetPage(name: NavPath.HOTELS, page: () => Hotels()));

    return _pages;
  }
}

class Navigation {
  static String _currentPath = '';

  static goto(String newPath, {Map args}) {
    //
    // Return if already on the requested Path.
    if (_currentPath == newPath) return;

    //
    // Only Load Splash Screen Once & Do not POP.
    if (newPath == NavPath.SPLASH && _currentPath == '') {
      _currentPath = NavPath.SPLASH;
      print('Goto SPLASH?');
      Get.toNamed(newPath);
      return;
    }

    // Go to New Screen.
    switch (newPath) {
      case NavPath.CITY_SEARCH:
        _currentPath = NavPath.CITY_SEARCH;
        Get.toNamed(NavPath.CITY_SEARCH);
        break;

      case NavPath.HOTELS:
        _currentPath = NavPath.HOTELS;
        Get.toNamed(NavPath.HOTELS, arguments: args);
        break;
    }
  }

  static goBack() {
    //
    // Return if user is on City or somehow on Splash.
    if (_currentPath == NavPath.SPLASH || _currentPath == NavPath.CITY_SEARCH)
      return;

    switch (_currentPath) {
      case NavPath.HOTELS:
        _currentPath = NavPath.CITY_SEARCH;
        Get.back();
        break;
    }
  }
}
