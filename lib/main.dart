import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:vegas_test/nav.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  runApp(VegasTest());
}

class VegasTest extends StatelessWidget {
  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
        title: 'Flutter Vegas Test',
        //navigatorKey: Get.key,
        initialRoute: NavPath.CITY_SEARCH,
        debugShowCheckedModeBanner: false,
        getPages: NavPath.pages());
  }
}
